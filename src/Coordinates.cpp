#include <iostream>
#include <memory>

#include "Coordinates.h"

using namespace std;

std::string GPSCoordinates::getSLongtitude()
{
	return "ttest";
}

std::string GPSCoordinates::getSLatitude()
{
	return "test";
}

double GPSCoordinates::getLongtitude()
{
	return _longtitude;
}

double GPSCoordinates::getLatitude()
{
	return _latitude;
}

void GPSCoordinates::setLongtitude(double longtitude)
{
	_longtitude = longtitude;
}

void GPSCoordinates::setLatitude(double latitude)
{
	_latitude = latitude;
}

void GPSCoordinates::setCoordinates(double longtitude, double latitude)
{
	_longtitude = longtitude;
	_latitude = latitude;
}

