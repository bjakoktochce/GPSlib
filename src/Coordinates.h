/*
 * Coordinates.h
 *
 *  Created on: Oct 29, 2017
 *      Author: Bartosz B. Jakoktochce
 */

#ifndef COORDINATES_H_
#define COORDINATES_H_


class GPSCoordinates
{
private:
	double _longtitude = 0.0;
	double _latitude = 0.0;
public:
	std::string getSLongtitude();
	std::string getSLatitude();
	double getLongtitude();
	double getLatitude();
	void setLongtitude(double);
	void setLatitude(double);
	void setCoordinates(double, double);
};


#endif /* COORDINATES_H_ */
