/*
 * GPS.cpp
 *
 *  Created on: Oct 29, 2017
 *      Author: Bartosz B. Jakoktochce
 */

#include <iostream>
#include <memory>
using namespace std;

#include "Coordinates.h"
#include "GPS.h"





void GPS::setLatitude(double latitude)
{
	_actualCoordinates.setLatitude(latitude);
}

void GPS::setLongtitude(double longtitude)
{
	_actualCoordinates.setLongtitude(longtitude);
}

double GPS::getLatitude()
{
	return _actualCoordinates.getLatitude();
}

double GPS::getLongtitude()
{
	return _actualCoordinates.getLongtitude();
}

void GPS::setCoordinates(GPSCoordinates coordinates)
{
	_actualCoordinates.setLongtitude(coordinates.getLongtitude());
	_actualCoordinates.setLatitude(coordinates.getLatitude());
}

GPSCoordinates GPS::getCoordinates()
{
	return _actualCoordinates;
}

