/*
 * GPS.h
 *
 *  Created on: Oct 29, 2017
 *      Author: Bartosz B. Jakoktochce
 */

#ifndef GPS_H_
#define GPS_H_

#include "Coordinates.h"

class GPS
{
private:
	GPSCoordinates _actualCoordinates;
public:
	GPSCoordinates updateCoordinates(GPSCoordinates);
	GPSCoordinates getCoordinates(void);
	void setLongtitude(double);
	void setLatitude(double);
	double getLatitude(void);
	double getLongtitude(void);
	void setCoordinates(GPSCoordinates);
};


#endif /* GPS_H_ */
