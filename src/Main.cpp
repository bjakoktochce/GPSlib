/*
 * Main.cpp
 *
 *  Created on: Oct 29, 2017
 *      Author: Bartosz B. Jakoktochce
 */

using namespace std;

#include <iostream>
#include <memory>

#include "Coordinates.h"
#include "GPS.h"

/* just for testing */
int main()
{
	std::shared_ptr<GPS> g (new GPS());
	GPSCoordinates coordinates;
	coordinates.setLatitude(0.2);
	coordinates.setLongtitude(-0.1);
	coordinates.setCoordinates(0.3, -0.5);
	g->setCoordinates(coordinates);



	printf("%f %f", g->getLongtitude(), g->getLatitude());



	return 0;
}

